import sys
import computeoo


class ComputeChild (computeoo.Compute):
    def __init__(self):
        self.default = 2

    def set_def(self):
        if sys.argv[3] <= 0:
            raise ValueError
        else:
            return sys.argv[3]

    def get_def(self):
        return self.default


if __name__ == "__main__":
    if len(sys.argv) < 3:
        sys.exit("Error: at least two arguments are needed")

    try:
        num = float(sys.argv[2])
    except ValueError:
        sys.exit("Error: second argument should be a number")

    if len(sys.argv) == 3:
        num2 = 2
    else:
        try:
            num2 = float(sys.argv[3])
        except ValueError:
            sys.exit("Error: third argument should be a number")

    if sys.argv[1] == "power":
        result = computeoo.Compute.power(num, num2)
    elif sys.argv[1] == "log":
        result = computeoo.Compute.log(num, num2)
    else:
        sys.exit('Operand should be power or log')

    print(result)
