import math
import sys


class Compute:
    def __init__(self):
        self.default = 2

    def power(self, default):
        return num ** default

    def log(self, default):
        return math.log(num, default)


if __name__ == "__main__":
    if len(sys.argv) < 3:
        sys.exit("Error: at least two arguments are needed")

    try:
        num = float(sys.argv[2])
    except ValueError:
        sys.exit("Error: second argument should be a number")

    if len(sys.argv) == 3:
        num2 = 2
    else:
        try:
            num2 = float(sys.argv[3])
        except ValueError:
            sys.exit("Error: third argument should be a number")

    if sys.argv[1] == "power":
        result = Compute.power(num, num2)
    elif sys.argv[1] == "log":
        result = Compute.log(num, num2)
    else:
        sys.exit('Operand should be power or log')

    print(result)
