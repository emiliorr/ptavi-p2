#!/usr/bin/python3
# -*- coding: utf-8 -*-

import math


class Compute:
    count = 0

    def __init__(self):
        self.default = 2

    def power(self, num1, num2=2):
        self.count += 1
        return num1 ** num2

    def log(self, num1, num2=2):
        self.count += 1
        return math.log(num1, num2)

    def set_def(self, num):
        self.count += 1
        if num <= 0:
            raise ValueError
        else:
            return num

    def get_def(self):
        self.count += 1
        return self.default
